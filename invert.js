/*
     Returns a copy of the object where the keys have become the values and the values the keys.
     Assume that all of the object's values will be unique and string serializable.
     http://underscorejs.org/#invert
*/

function invert(obj) {
	let invertedObj = {};
	for (items in obj) {
		invertedObj[obj[items]] = items;
	}
	return JSON.stringify(invertedObj);
}
module.exports = invert;
