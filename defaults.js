/*
 Fill in undefined properties that match properties on the `defaultProps` parameter object.
 Return `obj`.
 http://underscorejs.org/#defaults
*/
function defaults(obj, defaultProps) {
	for (items in defaultProps) {
		if (typeof obj[items] === "undefined") {
			obj[items] = defaultProps[items];
		}
	}
	return JSON.stringify(obj);
}
module.exports = defaults;
