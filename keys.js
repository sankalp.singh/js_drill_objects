/*
 Retrieve all the names of the object's properties.
 Return the keys as strings in an array.
 Based on http://underscorejs.org/#keys
*/
function keys(obj) {
	let objKeys = [];
	for (item in obj) {
		objKeys.push(item);
	}
	return JSON.stringify(objKeys);
}
module.exports = keys;
