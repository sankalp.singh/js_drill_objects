/*
 Convert an object into a list of [key, value] pairs.
 http://underscorejs.org/#pairs
*/
function pairs(obj) {
	let objPair = [];
	for (items in obj) {
		objPair.push([items, obj[items]]);
	}
	return JSON.stringify(objPair);
}

module.exports = pairs;
