const testObject = { name: "Bruce Wayne", age: 12, location: "Gotham" };
// const testObject = {};
const defaults = require("../defaults");

console.log(defaults(testObject, { age: 62, heroName: "Batman" }));
// console.log(defaults(testObject, { age: 2012 }));
// console.log(defaults());
// console.log(defaults(testObject));
// console.log(defaults({}));
