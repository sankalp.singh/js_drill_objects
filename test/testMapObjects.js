const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };
const mapObjects = require("../mapObjects");

// console.log(
// 	mapObjects(testObject, (elem) => {
// 		return elem * 2;
// 	})
// );
console.log(
	mapObjects(testObject, (elem) => {
		return elem + 2;
	})
);
