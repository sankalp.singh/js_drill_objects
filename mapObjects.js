/*
     Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
     http://underscorejs.org/#mapObject
*/
function mapObjects(obj, cb) {
	if (typeof cb === "undefined") {
		return obj;
	} else {
		for (items in obj) {
			obj[items] = cb(obj[items]);
		}
		return JSON.stringify(obj);
	}
}
module.exports = mapObjects;
