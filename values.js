/*
Return all of the values of the object's own properties.
 Ignore functions
 http://underscorejs.org/#values
*/

function values(obj) {
	let objValues = [];
	for (items in obj) {
		objValues.push(obj[items]);
	}
	return JSON.stringify(objValues);
}
module.exports = values;
